# pan-tool

A command line tool to create pan application and page easily

## install

npm install -g pan-tool
(you may need to be root)

## compile

clone this repo and then:
npm run build

## commands

### init pan conf

pan conf

### init a pan app

pan init

### publish a pan app

pan publish

### add data to your app

pan data add

### remove data to your app

pan data remove

### list data of your app

pan data list

### create page with app

pan page create

### publish page

pan page publish

## help (with command line options)

pan help
