import { SemVer, valid, coerce } from 'semver'
import { prompt, Question, Answers } from 'inquirer'
import { existsSync } from 'fs'

import { PromiseResolve, PromiseReject } from './promiseHelper'
import { Manifest } from './manifest';
import { keystoreUnlocked, sign } from './crypto'


// ################################
// Specifications
// ################################

// the ask* functions prompt a question if the answer is not in the cmd line
export let askConfigFolder: (defaultPath: string)                                           => Promise<string>
export let askPersonalInfo: (defaultConfig: any, cmdLineConfig?: any)                       => Promise<any>
export let askRemoveInfo:   (cmdLineConfig?: any, listName?: string[])                      => Promise<any>
export let askAddInfo:      (cmdLineConfig?: any, validateName?: (name: string) => boolean) => Promise<any>
export let askInfo:         (cmdLineConfig?: any)                                           => Promise<Manifest>
export let askFolder:       (defaultFolder?: string)                                        => Promise<string>
export let askApplicationIPFSHash:     ()                                                              => Promise<string>


//hash and sign the project in folder 'folder'
export let signProject: (store: keystoreUnlocked, manifest: Manifest, folder: string) => string



// ################################
// Private functions
// ################################

let validateVersion: (version: string) => boolean | string



// ################################
// Functions code
// ################################


validateVersion = (version:string): boolean | string =>
{
  if (valid(coerce(version) || ''))
  {
    return true
  }
  else
  {
    return 'Invalid version format'
  }
}

askConfigFolder = async (defaultPath: string): Promise<string> =>
{
  return new Promise<string>(async (resolve: PromiseResolve<string>, reject: PromiseReject) =>
  {
    let answer: Answers = await prompt([{message: 'Config folder: ', name: 'folder', default: defaultPath}])
    resolve (answer.folder)
  })
}

askPersonalInfo = async (defaultConfig: any, cmdLineConfig?: any): Promise<any> =>
{
  return new Promise<any>((resolve: PromiseResolve<any>, reject: PromiseReject) =>
  {
    let res: any = {}
    let questions: Question[] = new Array<Question>()

    let q1: Question = {message: 'Pseudo: ', name: 'pseudo', default: defaultConfig.pseudo || 'Batman'}
    if (cmdLineConfig && cmdLineConfig.pseudo)
    {
      res.pseudo = cmdLineConfig.pseudo
    }
    else
    {
      questions.push(q1)
    }
    let q2: Question = {message: 'Description: ', name: 'description', default: defaultConfig.description || ''}
    if (cmdLineConfig && cmdLineConfig.description)
    {
      res.description = cmdLineConfig.description
    }
    else
    {
      questions.push(q2)
    }
    prompt(questions).then((answer: Answers): void =>
    {
      res.pseudo = res.pseudo || answer.pseudo
      res.description = res.description || answer.description
      resolve(res)
    })
  })
}

askRemoveInfo = async (cmdLineConfig?: any, listName?: string[]): Promise<any>  =>
{
  return new Promise<any>((resolve: PromiseResolve<any>, reject: PromiseReject) =>
  {
    let res: any = {}
    let questions: Question[] = new Array<Question>()

    let q1: Question = {message: 'Data name: ', name: 'name', choices: (listName || []), type: 'list'}

    if (cmdLineConfig && cmdLineConfig.name && (!listName || listName.find((value: any) => {return value.name === cmdLineConfig.name})))
    {
      res.name = cmdLineConfig.name
    }
    else
    {
      questions.push(q1)
    }

    prompt(questions).then((answer: Answers): void =>
    {
      if (answer.name)
      {
        res.name = answer.name
      }
      resolve(res)
    })
  })
}

askAddInfo = async (cmdLineConfig?: any, validateName?: (name: string) => boolean): Promise<any>  =>
{
  return new Promise<any>((resolve: PromiseResolve<any>, reject: PromiseReject) =>
  {
    let res: any = {}
    let questions: Question[] = new Array<Question>()

    let q1: Question = {message: 'Data name: ', name: 'name', validate: (validateName || ((name: string): boolean => { return true}))}
    let q2: Question = {message: 'Data description: ', name: 'description'}
    let q3: Question = {message: 'Data type: ', name: 'type', default: 'text', type: 'list', choices: ['text', 'text-short', 'image', 'video', 'bin'] }

    if (cmdLineConfig && cmdLineConfig.name && (!validateName || validateName(cmdLineConfig.name)))
    {
      res.name = cmdLineConfig.name
    }
    else
    {
      questions.push(q1)
    }

    if (cmdLineConfig && cmdLineConfig.description)
    {
      res.description = cmdLineConfig.description
    }
    else
    {
      questions.push(q2)
    }

    if (cmdLineConfig && cmdLineConfig.type)
    {
      res.entreyPoint = cmdLineConfig.type
    }
    else
    {
      questions.push(q3)
    }

    prompt(questions).then((answer: Answers): void =>
    {
      if (answer.name)
      {
        res.name = answer.name
      }
      if (answer.description)
      {
        res.description = answer.description
      }
      if (answer.type)
      {
        res.type = answer.type
      }
      resolve(res)
    })
  })
}

askInfo = async (cmdLineConfig?: any): Promise<Manifest>  =>
{
  return new Promise<Manifest>((resolve: PromiseResolve<Manifest>, reject: PromiseReject) =>
  {
    let res: Manifest = new Manifest()
    let questions: Question[] = new Array<Question>()

    let q1: Question = {message: 'Application name: ', name: 'name'}
    let q2: Question = {message: 'Application version: ', name: 'version', validate: validateVersion, default: '1.0.0'}
    let q3: Question = {message: 'Application entry point: ', name: 'entryPoint', default: 'index.html'}

    if (cmdLineConfig && cmdLineConfig.name)
    {
      res.name = cmdLineConfig.name
    }
    else
    {
      questions.push(q1)
    }

    if (cmdLineConfig && cmdLineConfig.version)
    {
      res.version = new SemVer(coerce(cmdLineConfig.version) || '1.0.0')
    }
    else
    {
      questions.push(q2)
    }

    if (cmdLineConfig && cmdLineConfig.entryPoint)
    {
      res.entreyPoint = cmdLineConfig.entryPoint
    }
    else
    {
      questions.push(q3)
    }

    prompt(questions).then((answer: Answers): void =>
    {
      if (answer.name)
      {
        res.name = answer.name
      }
      if (answer.version)
      {
        res.version = new SemVer(coerce(answer.version) || '1.0.0')
      }
      if (answer.entreyPoint)
      {
        res.entreyPoint = answer.entreyPoint
      }
      resolve(res)
    })
  })
}

askFolder = async (defaultFolder?: string): Promise<string> =>
{
  return new Promise<string>(async (resolve: PromiseResolve<string>, reject: PromiseReject) =>
  {
    let folderPath: string = defaultFolder || (await prompt([{message: 'Folder with code: ', name: 'folder', validate:existsSync, default: './'}]) as Answers).folder
    if (!folderPath)
    {
      folderPath = './'
    }

    if (folderPath[folderPath.length - 1] != '/')
    {
      folderPath = folderPath + '/'
    }

    resolve(folderPath)
  })
}

askApplicationIPFSHash = async (): Promise<string> =>
{
  return new Promise<string>(async (resolve: PromiseResolve<string>, reject: PromiseReject) =>
  {
    let cid: string = (await prompt([{message: 'IPFS application: ', name: 'cid', default: '', validate: ((value: string) => { return value.length === 46 || value.length === 0})}]) as Answers).cid
    resolve(cid)
  })
}


signProject = (store: keystoreUnlocked, manifest: Manifest, folder: string): string =>
{
  return sign(store.store, store.pwddk, manifest, folder)
}
