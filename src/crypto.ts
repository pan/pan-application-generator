import { keystore, signing } from 'eth-lightwallet'
import { prompt, Question, Answers } from 'inquirer'
import { createHash, Hash } from 'crypto'
import { readdirSync, readFileSync, Stats, statSync} from 'fs'

import { PromiseResolve, PromiseReject } from './promiseHelper'
import { Manifest } from './manifest'



// ################################
// Specifications
// ################################

//this type represents a keystore with the derivated password used for uncipher the private key
//this should not be saved nor print in terminal
export type keystoreUnlocked = {store: keystore, pwddk: Uint8Array}



// return a new keystore using the password 'pwd' to cipher the private key.
// if the parameter 'pwd' is omited then the password is asked (and that's better this way)
export let getKeystore: (pwd?: string | null) => Promise<keystoreUnlocked>

// return a JSON encoded keystore ready to be saved on file system
export let keystoreToJson: (ks: keystore) => string
// return a keystore from a previously saved keystore
export let keystoreFromJson: (json: string) => keystore

// return a keystore unlocked by the password 'pwd' or the password asked on the terminal
export let UnlockKeystore: (ks: keystore, pwd?: string) => Promise<keystoreUnlocked>


// sign he manifest and everything in the folder (except 'signature.hex', 'manifest.json')
export let sign: (store: keystore, pwddk: Uint8Array, manifest: Manifest, folder: string, keyNum?: number) => string




// ################################
// Private functions
// ################################


// hash the manifest and everything in the folder (except 'signature.hex', 'manifest.json')
let hash : (manifest: Manifest, folder: string) => Buffer


// ################################
// Functions code
// ################################



getKeystore = async (pwd?: string | null): Promise<keystoreUnlocked> =>
{
  return new Promise<keystoreUnlocked>(async (resolve: PromiseResolve<keystoreUnlocked>, reject: PromiseReject) =>
  {
    let password: string = ""

    password = pwd || (await prompt([{message: "Keystore password: ", name: "password", type: "password"}]) as Answers).password

    keystore.createVault(
    {
      password: password,
      seedPhrase: keystore.generateRandomSeed(),
      hdPathString: 'm/0\'/0\'/0\'',
    },
    function (err: any, ks: keystore)
    {
      if (err)
      {
        reject (err)
      }
      ks.keyFromPassword(password, function (err, pwDerivedKey)
      {
        if (err)
        {
          reject (err)
        }

        ks.generateNewAddress(pwDerivedKey, 1)
        resolve ({store: ks, pwddk: pwDerivedKey})
      })
    })
  })
}

keystoreFromJson =  (json: string): keystore =>
{
  return keystore.deserialize(json)
}
keystoreToJson=  (ks: keystore): string =>
{
  return ks.serialize()
}

UnlockKeystore = async (ks: keystore, pwd?: string): Promise<keystoreUnlocked> =>
{
  return new Promise<keystoreUnlocked>(async (resolve: PromiseResolve<keystoreUnlocked>, reject: PromiseReject) =>
  {
    let password: string = pwd || (await prompt([{message: "Keystore password: ", name: "password", type: "password"}]) as Answers).password

      ks.keyFromPassword(password, function (err, pwDerivedKey)
      {
        if (err)
        {
          reject (err)
        }

        ks.generateNewAddress(pwDerivedKey, 1)
        resolve ({store: ks, pwddk: pwDerivedKey})
      })
    })
}


hash = (manifest: Manifest, folder: string):  Buffer =>
{
  let hasher: Hash =  createHash('sha256')
  hasher.update(manifest.toJSON(null, 4))
  readdirSync(folder).forEach((file: string):void =>
  {
    if (!statSync(folder + file).isDirectory() && file !== 'signature.hex' && file !== 'manifest.json')
    {
      console.log('adding ' + folder + file + '...')
      hasher.update(readFileSync(folder + file))
    }
  })

  return hasher.digest()
}


sign = (store: keystore, pwddk: Uint8Array, manifest: Manifest, folder: string, keyNum?: number): string =>
{
  if (!keyNum)
  {
    keyNum = 0
  }
  return signing.concatSig(signing.signMsg(store, pwddk, hash(manifest, folder).toString(), store.getAddresses()[keyNum]))
}
