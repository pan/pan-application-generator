#!/usr/bin/env node
import { keystore } from 'eth-lightwallet'
let params = require('parameters')
import { existsSync, mkdirSync, readFileSync, writeFileSync, unlinkSync, copyFileSync } from 'fs'
import { relative, basename } from 'path'
import * as WebTorrent from 'webtorrent'
import { prompt, Question, Answers } from 'inquirer'


import { PromiseResolve, PromiseReject } from './promiseHelper'
import { Manifest } from './manifest';
import { askInfo, askFolder, signProject, askConfigFolder, askPersonalInfo, askAddInfo, askRemoveInfo, askApplicationIPFSHash } from './applicationCreator'
import { keystoreUnlocked, getKeystore, UnlockKeystore, keystoreToJson, keystoreFromJson } from './crypto'
import { file, files, initIPFS, addToIPFS, IPFSGet } from './ipfs'
import { createTorrentFromPath, getInfoHash, writeTorrentFile } from './torrent'


// ################################
// Specifications
// ################################

// Get parameters from command line and return it
// for now it is just the command without options
// => return the command or null if (no command or help command)
let getParam: () => any | null

// Run the command asked in the command line
let runConfig: (defaultValues: any) => Promise<void>
let runInit: (defaultValues: any) => Promise<void>
let runPublish: (defaultValues: any) => Promise<void>
let runData: (defaultValues: any) => Promise<void>
let runPage: (defaultValues: any) => Promise<void>

// The main function
// this function is run at the end of the file
let run: () => void

// ################################
// Functions code
// ################################

getParam = (): any | null =>
{
  let command = params(
    {
      name: 'pan',
      description: 'Tool for Pan application creation and publication',
      commands:
      [
        {
          name: 'config', shortcut: 'c',
          description: 'Manage tool configuration',
          options:
          [
            {
              name: 'path', shortcut: 'p',
              description: 'Path of the config folder',
            },
            {
              name: 'pseudo', shortcut: 'ps',
              description: 'Your pseudonyme',
            },
            {
              name: 'description', shortcut: 'd',
              description: 'Your personal description',
            },
            {
              name: 'password', shortcut: 'pwd',
              description: 'Password to unlock the keystore. Be careful this may save your password in the command history and then be unsafe',
            },
          ]
        },
        {
          name: 'init', shortcut: 'i',
          description: 'Initialize a Pan application',
          options:
          [
            {
              name: 'path', shortcut: 'p',
              description: 'Path of the code of the application',
            },
            {
              name: 'name', shortcut: 'n',
              description: 'Name of the application',
            },
            {
              name: 'version', shortcut: 'v',
              description: 'Version of the application',
            },
            {
              name: 'entryPoint', shortcut: 'e',
              description: 'Entry point of the application',
            },
          ]
        },
        {
          name: 'publish', shortcut: 'p',
          description: 'Publish a Pan application',
          options:
          [
            {
              name: 'configPath', shortcut: 'cp',
              description: 'Path of the config folder',
            },
            {
              name: 'applicationPath', shortcut: 'ap',
              description: 'Path of the code of the application',
            },
            {
              name: 'password', shortcut: 'pwd',
              description: 'Password to unlock the keystore. Be careful this may save your password in the command history and then be unsafe',
            },
          ],
          commands:
          [
            {
              name: 'keep', shortcut: 'k',
              description: 'Keep generated file in the code folder after publish'
            },
          ],
        },
        {
          name: 'data', shortcut: 'a',
          description: 'Manage data of your application',
          commands:
          [
            {
              name: 'add', shortcut: 'a',
              description: 'Add data',
              options:
              [
                {
                  name: 'path', shortcut: 'p',
                  description: 'Path of project',
                  default: './',
                },
                {
                  name: 'name', shortcut: 'n',
                  description: 'Name of the new data',
                },
                {
                  name: 'description', shortcut: 'd',
                  description: 'Short description of the new data. This will help users create a page.',
                },
                {
                  name: 'type', shortcut: 't',
                  description: 'Type of data',
                  one_of: ['text', 'text-short', 'image', 'video', undefined],
                },
              ],
            },
            {
              name: 'remove', shortcut: 'r',
              description: 'remove data',
              options:
              [
                {
                  name: 'path', shortcut: 'p',
                  description: 'Path of project',
                  default: './',
                },
                {
                  name: 'name', shortcut: 'n',
                  description: 'Name of the data to remove',
                },
              ],
            },
            {
              name: 'list', shortcut: 'l',
              description: 'list data',
            },
          ],
        },
        {
          name: 'page', shortcut: 'p',
          description: 'Manage Pan page',
          commands:
          [
            {
                name: 'create', shortcut: 'c',
                description: 'create a new page',
                options:
                [
                  {
                      name: 'app', shortcut: 'a',
                      description: 'Application used as base to create the page. If "" is specified then the page will have no base',
                  },
                  {
                    name: 'interactive', shortcut: 'i',
                    description: 'Should the data field filled with the question in the terminal or by editing the data.json?',
                    type: 'boolean'
                  },
                ],
            },
            {
                name: 'publish', shortcut: 'p',
                description: 'publish a page',
                options:
                [
                  {
                    name: 'configPath', shortcut: 'cp',
                    description: 'Path of the config folder',
                  },
                  {
                    name: 'pagePath', shortcut: 'pp',
                    description: 'Path of the code of the page',
                  },
                  {
                    name: 'password', shortcut: 'pwd',
                    description: 'Password to unlock the keystore. Be careful this may save your password in the command history and then be unsafe',
                  },
                ],
            },
          ],
        },
      ]
    })
    let args = command.parse()
    let commandHelp = command.helping(args)
    if (commandHelp)
    {
      process.stdout.write(command.help(commandHelp));
      return null
    }
    else
    {
      return args
    }
}


runConfig = async (defaultValues: any): Promise<void> =>
{
  let configFolder:string = defaultValues.path || process.env.PAN_CONFIG_PATH || await askConfigFolder(process.env.HOME + '/.pan/')

  if (configFolder != process.env.PAN_CONFIG_PATH && configFolder != process.env.HOME + '/.pan/')
  {
    console.info('You can set the environement variable PAN_CONFIG_PATH to \'' + configFolder + '\' in order to not have to retype this path ever again.')
  }

  let configPath: string = configFolder + 'pan.json'
  let cryptoConfigPath: string = configFolder + 'keystore.json'

  let config: any = {}
  let keystore: any = {}

  if (!existsSync(configFolder))
  {
    mkdirSync(configFolder)
  }

  if (existsSync(configPath))
  {
    config = JSON.parse(readFileSync(configPath).toString())
  }

  config = await askPersonalInfo(config, defaultValues)


  if (existsSync(cryptoConfigPath))
  {
    keystore = keystoreFromJson(readFileSync(cryptoConfigPath).toString())
  }
  else
  {
    let ks: keystoreUnlocked = await getKeystore(defaultValues.password)
    keystore = ks.store
  }

  config.publicAddress = (keystore as keystore).getAddresses()[0]

  writeFileSync(configPath, JSON.stringify(config, null, 4))
  writeFileSync(cryptoConfigPath, keystoreToJson(keystore))

  console.log('Your public address is ' + config.publicAddress)
  console.log('Config created in ' + configFolder)

}

runInit = async (defaultValues: any): Promise<void> =>
{
  let manifest: Manifest = await askInfo(defaultValues)
  let folder: string = await askFolder(defaultValues.path)

  writeFileSync(folder + 'manifest.json', manifest.toJSON(null, 4))
  writeFileSync(folder + 'data.json', JSON.stringify([], null, 4))


  console.log('Project initialized in folder \'' + folder + '\'')
}

runPublish = async (defaultValues: any): Promise<void> =>
{
  let configFolder:string = defaultValues.configPath || process.env.PAN_CONFIG_PATH || process.env.HOME + '/.pan/'

  let configPath: string = configFolder + 'pan.json'
  let cryptoConfigPath: string = configFolder + 'keystore.json'

  if (!existsSync(configPath) || !existsSync(cryptoConfigPath))
  {
    console.log('You should run the command \'pan config\' before this command')
    return
  }

  let folder: string  = defaultValues.applicationPath ||  './'
  while (!existsSync(folder + 'manifest.json'))
  {
    folder = await askFolder()
  }
  let manifestPath: string = folder + 'manifest.json'
  let manifest: Manifest = new Manifest(readFileSync(manifestPath).toString())

  let signaturePath = folder + 'signature.hex'
  if (existsSync(signaturePath))
  {
    unlinkSync(signaturePath)
  }
  let torrentPath: string = folder + 'manifest.torrent'
  if (existsSync(torrentPath))
  {
    unlinkSync(torrentPath)
  }


  let ks: keystoreUnlocked = await UnlockKeystore(keystoreFromJson(readFileSync(cryptoConfigPath).toString()), defaultValues.password)
  if (manifest.signingAddress === '')
  {
    manifest.signingAddress = ks.store.getAddresses()[0]
    writeFileSync(manifestPath, manifest.toJSON(null, 4))
  }

  let signature: string = signProject(ks, manifest, folder)
  writeFileSync(signaturePath, signature)

  let torrent: WebTorrent.TorrentFile = await createTorrentFromPath(folder)
  writeTorrentFile(torrent, torrentPath)
  let torrentHash: string = getInfoHash(torrent)
  console.log('Application torrent hash is ' + torrentHash)

  await initIPFS()
  let ipfsHash: string = await addToIPFS(folder)

  console.log("Application published at " + ipfsHash)

  if (typeof(defaultValues.command) === 'string' || defaultValues.command[1] !== 'keep')
  {
    if (existsSync(signaturePath))
    {
      unlinkSync(signaturePath)
    }
    if (existsSync(torrentPath))
    {
      unlinkSync(torrentPath)
    }
  }

}


runData = async (defaultValues: any): Promise<void> =>
{
  if (typeof defaultValues.command == 'string')
  {
    console.error('Syntax error: you should add a {add, remove, list} command to the data command')
    return
  }


  let addAction: () => Promise<void> = async (): Promise<void> =>
  {
    let folder: string  = defaultValues.path ||  './'
    while (!existsSync(folder + 'data.json'))
    {
      folder = await askFolder()
    }

    let dataPath: string = folder + 'data.json'
    let dataJSON: any = JSON.parse(readFileSync(dataPath).toString())

    let isNameUniq: (name: string) => boolean = (name: string): boolean =>
    {
      for (let i: number = 0; i < dataJSON.length; i++)
      {
        if (dataJSON[i].name === name)
        {
          console.log('')
          console.error('This name already exist.')
          return false
        }
      }
      return true
    }

    let info: any = await askAddInfo(defaultValues, isNameUniq)


    dataJSON.push({name: info.name, description: info.description, type: info.type})

    writeFileSync(dataPath, JSON.stringify(dataJSON, null, 4))
  }

  let removeAction: () => Promise<void> = async (): Promise<void> =>
  {
    let folder: string  = defaultValues.path ||  './'
    while (!existsSync(folder + 'data.json'))
    {
      folder = await askFolder()
    }

    let dataPath: string = folder + 'data.json'
    let dataJSON: any = JSON.parse(readFileSync(dataPath).toString())

    let listName: string[] = new Array<string>()

    for (let i: number = 0; i < dataJSON.length; i++)
    {
      listName.push(dataJSON[i].name)
    }

    let info: any = await askRemoveInfo(defaultValues, listName)

    dataJSON.splice(dataJSON.findIndex((value: any) => { return value.name === info.name }), 1)

    writeFileSync(dataPath, JSON.stringify(dataJSON, null, 4))
  }

  let listAcction: () => Promise<void> = async (): Promise<void> =>
  {
    let folder: string  = defaultValues.path ||  './'
    while (!existsSync(folder + 'data.json'))
    {
      folder = await askFolder()
    }

    let dataPath: string = folder + 'data.json'
    let dataJSON: any = JSON.parse(readFileSync(dataPath).toString())

    console.log(JSON.stringify(dataJSON, null, 4))

  }

  switch (defaultValues.command[1])
  {
    case 'add':
      addAction()
      break
    case 'remove':
      removeAction()
      break
    case 'list':
      listAcction()
      break
    default: return
  }
}

runPage = async (defaultValues: any): Promise<void> =>
{
  if (typeof defaultValues.command == 'string')
  {
    console.error('Syntax error: you should add a {create} command to the page command')
    return
  }


  let createAction: (param: any) => Promise<string> = async (): Promise<string> =>
  {
    return new Promise(async (resolve: PromiseResolve<string>, reject: PromiseReject) =>
    {

      initIPFS()

      let folder: string  = defaultValues.path ||  ''
      while (folder === '' || !existsSync(folder))
      {
        folder = await askFolder()
      }



      let cid: string = defaultValues.app || await askApplicationIPFSHash()

      let app: files | null = await IPFSGet(cid)
      while (!app)
      {
        console.log('Impossible to find ' + cid)
        cid = await askApplicationIPFSHash()
        app = await IPFSGet(cid)
      }
      let manifest:   file | null = app.find((value: file) => { return value.path === cid + '/manifest.json' }) || null
      let data:       file | null = app.find((value: file) => { return value.path === cid + '/data.json' })     || null
      let signature:  file | null = app.find((value: file) => { return value.path === cid + '/signature.hex' }) || null

      //todo check signature

      if (!manifest || !manifest.content)
      {
        resolve('Application format error')
      }

      let parsedManifest: Manifest = new Manifest((manifest as file).content.toString())
      if (parsedManifest.name)
      {
        console.log('Application ' + parsedManifest.name)
      }
      else
      {
        console.log('Application without name')
      }

      if (!params.interactive)
      {
        resolve('')
      }


      if (!data || !data.content)
      {
        console.log('This application does not need data')
        resolve('')
      }

      let dataParsed: any[] =  JSON.parse((data as file).content.toString())

      let initURL: (url: string) => string = (url: string): string =>
      {
        if (relative('', url)[0] === '.')
        {
          let filePath: string = './data/' + basename(url)
          if (!existsSync('./data/'))
          {
            mkdirSync('./data')
          }
          copyFileSync(url, filePath)
          return filePath
        }
        else
        {
          return url
        }
      }

      let res: any[] = new Array()

      for (let d of dataParsed)
      {
        switch (d.type)
        {
          case 'text':
            res.push(Object.assign(d, {value: (await prompt([{message: (d.name || '') + ' (type text): ', name: 't'}])as Answers).t}))
            break
          case 'text-short':
          res.push(Object.assign(d, {value: (await prompt([{message: (d.name || '') + ' (type text-short): ', name: 't'}])as Answers).t}))
            break
          case 'image':
          res.push(Object.assign(d, {value: initURL((await prompt([{message: (d.name || '') + ' (type image path): ', name: 't'}])as Answers).t)}))
            break
          case 'video':
          res.push(Object.assign(d, {value: initURL((await prompt([{message: (d.name || '') + ' (type video path): ', name: 't'}])as Answers).t)}))
            break
          case undefined:
            //free fall
          default:
          res.push(Object.assign(d, {value: initURL((await prompt([{message: (d.name || '') + ' (type data path): ', name: 't'}])as Answers).t)}))
        }
      }

      writeFileSync(folder + 'data.json', JSON.stringify(res, null, 4))
      writeFileSync(folder + 'manifest.json', JSON.stringify({application: cid}, null, 4))

    })
  }


  let publishAction: () => Promise<void> = async (): Promise<void> =>
  {
    let configFolder:string = defaultValues.configPath || process.env.PAN_CONFIG_PATH || process.env.HOME + '/.pan/'

    let configPath: string = configFolder + 'pan.json'
    let cryptoConfigPath: string = configFolder + 'keystore.json'

    if (!existsSync(configPath) || !existsSync(cryptoConfigPath))
    {
      console.log('You should run the command \'pan config\' before this command')
      return
    }

    let folder: string  = defaultValues.pagePath ||  './'
    while (!existsSync(folder + 'manifest.json'))
    {
      folder = await askFolder()
    }
    let manifestPath: string = folder + 'manifest.json'
    let manifest: Manifest = new Manifest(readFileSync(manifestPath).toString())

    let signaturePath = folder + 'signature.hex'
    if (existsSync(signaturePath))
    {
      unlinkSync(signaturePath)
    }
    let torrentPath: string = folder + 'manifest.torrent'
    if (existsSync(torrentPath))
    {
      unlinkSync(torrentPath)
    }


    let ks: keystoreUnlocked = await UnlockKeystore(keystoreFromJson(readFileSync(cryptoConfigPath).toString()), defaultValues.password)
    if (manifest.signingAddress === '')
    {
      manifest.signingAddress = ks.store.getAddresses()[0]
      writeFileSync(manifestPath, manifest.toJSON(null, 4))
    }

    let signature: string = signProject(ks, manifest, folder)
    writeFileSync(signaturePath, signature)

    let torrent: WebTorrent.TorrentFile = await createTorrentFromPath(folder)
    writeTorrentFile(torrent, torrentPath)
    let torrentHash: string = getInfoHash(torrent)
    console.log('Page torrent hash is ' + torrentHash)

    await initIPFS()
    let ipfsHash: string = await addToIPFS(folder)

    console.log("Page published at " + ipfsHash)

    if (typeof(defaultValues.command) === 'string' || defaultValues.command[1] !== 'keep')
    {
      if (existsSync(signaturePath))
      {
        unlinkSync(signaturePath)
      }
      if (existsSync(torrentPath))
      {
        unlinkSync(torrentPath)
      }
    }
  }

  switch (defaultValues.command[1])
  {
    case 'create':
      let res: string = await createAction(defaultValues)
      if (res)
      {
        console.error(res)
      }
      break
    case 'publish':
      await publishAction()
      break
    default: return
  }
}



run = async () =>
{
  let panCommand: any | null = getParam()

  if (panCommand == null)
  {
    return
  }

  let command: string
  if (typeof(panCommand.command) === 'string')
  {
    command = panCommand.command
  }
  else
  {
    command = panCommand.command[0]
  }

  switch (command)
  {
    case 'config':
      runConfig(panCommand)
      break
    case 'init':
      runInit(panCommand)
      break
    case 'publish':
      runPublish(panCommand)
      break
    case 'data':
      runData(panCommand)
      break
    case 'page':
      runPage(panCommand)
      break
    default: return
  }

}

run()
