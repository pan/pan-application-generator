let ipfsAPI = require('ipfs-api')
import { readFile } from 'fs'

import { PromiseResolve, PromiseReject } from './promiseHelper'




// ################################
// Specifications
// ################################

export type file = { path: string, content: Buffer}
export type files = file[]

// should be done once before everything else
export let initIPFS: () => void
// add everithing int the folder 'path' and return the ipfs hash
export let addToIPFS: (path: string) => Promise<string>
// get from ipfs
export let IPFSGet: (id: string) => Promise<file[]>

// ################################
// private members
// ################################

let ipfs: any = null

// ################################
// Functions code
// ################################


initIPFS = ():void =>
{
  ipfs = ipfsAPI('localhost', '5001')
}

addToIPFS = (path: string): Promise<string> =>
{
  return new Promise((resolve: PromiseResolve<string>, reject: PromiseReject) =>
  {

    ipfs.util.addFromFs(path,
    {
      recursive: true
    }, (err: any, resIPFS: any) =>
    {
      if (err)
      {
        reject(err)
      }
      resolve(resIPFS[resIPFS.length - 1].hash)
    })

  })
}

IPFSGet = (id: string): Promise<file[]> =>
{
  return new Promise((resolve: PromiseResolve<file[]>, reject: PromiseReject) =>
  {
    ipfs.get(id, (err: any, files: file[]) =>
    {
      if (err)
      {
        reject(err)
      }
      else
      {
        resolve(files)
      }
    })
  })
}
