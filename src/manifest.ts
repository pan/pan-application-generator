import { SemVer } from 'semver'


export class Manifest
{
  name: string = ''
  version: SemVer = new SemVer('0.0.0')
  entreyPoint: string = 'index.html'
  signingAddress: string = ''

  public constructor (json?: string)
  {
    if (json)
    {
      let tmp: any = JSON.parse(json)
      this.name = tmp.name || ''
      this.version = new SemVer(tmp.version || '0.0.0')
      this.entreyPoint = tmp.entreyPoint || 'index.html'
      this.signingAddress = tmp.signingAddress || ''
    }
  }



  public toJSON: (param1?: any, param2?: any) => string = (param1?: any, param2?: any): string =>
  {
    interface ManifestString
    {
      name: string,
      version: string,
      entryPoint: string,
      signingAddress: string,
    }
    let res: ManifestString =
    {
      name: this.name,
      version: this.version.toString(),
      entryPoint: this.entreyPoint,
      signingAddress: this.signingAddress,
    }
    if (param1 || param2)
    {
      return JSON.stringify(res, param1, param2)
    }
    else
    {
        return JSON.stringify(res)
    }
  }



}
