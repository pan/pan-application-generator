let createTorrent = require('create-torrent')
import * as WebTorrent from 'webtorrent'
import * as ParseTorrent from 'parse-torrent'
import { writeFileSync } from 'fs'

import { PromiseResolve, PromiseReject } from './promiseHelper'


// ################################
// Specifications
// ################################

export let createTorrentFromPath: (path: string) => Promise<WebTorrent.TorrentFile>
export let getInfoHash: (torrent: WebTorrent.TorrentFile) => string
export let writeTorrentFile: (torrent: WebTorrent.TorrentFile, path: string) => void


// ################################
// Functions code
// ################################

createTorrentFromPath = (path: string): Promise<WebTorrent.TorrentFile> =>
{
  return new Promise<WebTorrent.TorrentFile>((resolve: PromiseResolve<WebTorrent.TorrentFile>, reject: PromiseReject): void =>
  {
    createTorrent(path, (err: any, torrent: WebTorrent.TorrentFile) =>
    {
      resolve (torrent)
    })
  })

}

getInfoHash = (torrent: WebTorrent.TorrentFile): string =>
{
  return  ParseTorrent.default(torrent).infoHash
}

writeTorrentFile = (torrent: WebTorrent.TorrentFile, path: string): void =>
{
  writeFileSync(path, torrent)
}
