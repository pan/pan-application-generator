import { askInfo, askFolder } from '../src/applicationCreator'
import { getKeystore, keystoreUnlocked, sign } from '../src/crypto'
import { expect } from 'chai'
import { Manifest } from '../src/manifest'
import { SemVer } from 'semver'
import { keystore } from 'eth-lightwallet'

import { PromiseResolve, PromiseReject } from '../src/promiseHelper'



import 'mocha'
var robot = require("robotjs");


let sleep: (time: number) => void = async (time: number) =>
{
	await new Promise<boolean>((resolve: PromiseResolve<boolean>, reject: PromiseReject) =>
	{
		setTimeout(()=>{resolve(true)}, time)
	})

}

let genManifest: () => Promise<Manifest> = async () : Promise<Manifest> =>
{
	  return new Promise<Manifest>(async (resolve: PromiseResolve<Manifest>, reject: PromiseReject) =>
		{
			let res: Promise<Manifest> = askInfo()

			await sleep(10)
			robot.typeString('name')
			robot.keyTap("enter")
			await sleep(10)
			robot.typeString('1.2.3')
			robot.keyTap("enter")
			await sleep(10)
			robot.keyTap("enter")

			let manifest: Manifest = await res
			expect(manifest).to.not.be.empty
			expect(manifest.name).to.be.eq('name')
			expect(JSON.stringify(manifest.version)).to.be.eq(JSON.stringify(new SemVer('1.2.3')))
			resolve (manifest)
		})
}

let genKeystore: () => Promise<keystoreUnlocked> = async () : Promise<keystoreUnlocked> =>
{
	  return new Promise<keystoreUnlocked>(async (resolve: PromiseResolve<keystoreUnlocked>, reject: PromiseReject) =>
		{
			let res: Promise<keystoreUnlocked> = getKeystore()

			await sleep(10)
			robot.typeString('password')
			robot.keyTap("enter")


			let ks: keystoreUnlocked = await res
			expect(ks).to.not.be.empty
			expect(ks.store).to.not.be.empty
			expect(ks.pwddk).to.not.be.empty
			expect(ks.store.getAddresses()[0]).to.be.a('string')
			resolve (ks)
		})
}

let getFolder: () => Promise<string> = async () : Promise<string> =>
{
	  return new Promise<string>(async (resolve: PromiseResolve<string>, reject: PromiseReject) =>
		{
			let res: Promise<string> = askFolder()

			await sleep(10)
			robot.typeString('./test/testApp1')
			robot.keyTap("enter")


			let folder: string = await res
			expect(folder).to.not.be.empty
			resolve (folder)
		})
}


describe('generate function', () =>
{

	it('should create a manifest', async () =>
	{
		await genManifest()
	})

	it('should create a keystore', async () =>
	{
		await genKeystore()
	})
	it('should ask for a folder', async () =>
	{
		await getFolder()
	})

	it('should create a keystore and sign manifest', async () =>
	{
		let ks: keystoreUnlocked = await genKeystore()
		let manifest: Manifest = await genManifest()
		let folder: string = await getFolder()

		let signature: string = sign(ks.store, ks.pwddk, manifest, folder)
		expect(signature).to.not.be.empty
	})

})
